
<!-- ========================= SECTION MAIN ========================= -->
<section class="section-main bg padding-y-sm">
	<div class="container">
	<header class="section-heading heading-line">
						<h4 class="title-section bg text-uppercase">RESULTADOS PARA: <?php echo $_GET['encontrar']; ?></h4>
					</header>
		<div class="card">
			<div class="card-body">
				<div class="row row-sm">
					
					<aside class="col-md-3">
					</aside>
						<!-- ================= BUSQUEDA ================= -->
							<div class="container">
								<div class="row">
									<?php
										$busqueda = $_GET['encontrar'];

										include("administracion/login/conexion.php");

										$consulta = "
											SELECT * FROM productos
											WHERE product_name
											LIKE '%$busqueda%'
											UNION SELECT * FROM productos
											WHERE product_category
											LIKE '%$busqueda%'
											UNION SELECT * FROM productos
											WHERE product_description
											LIKE '%$busqueda%'
											";
											$resultado = $conexion->query($consulta);

											if (empty($busqueda)) {
												echo "El recuadro de busqueda no puede estar vacío, escriba algo porfavor c:";
											}else{
												if($resultado->num_rows > 0){
													while ( $fila = $resultado->fetch_assoc()){?>
														<div class="col-md-4">
															<figure class="card card-product">
																<div class="img-wrap"><a href="../categorias/producto.php?id=<?php echo $fila['id'];?>"><img src="data:image/jpg;base64,<?php echo base64_encode($fila['product_img']);?>"></a>
																</div>
																<figcaption class="info-wrap">
																	<h4 class="title" style="text-transform: uppercase;"><?php echo $fila['product_name']; ?></h4>
																		<p style="text-transform: uppercase;"><span>Categoría: </span><?php echo $fila['product_category']; ?></p>
																		<p class="desc" style="text-transform: uppercase;"><?php echo $fila['product_description']; ?></p>
																		<div class="rating-wrap">
																			<ul class="rating-stars">
																				<li style="width:80%" class="stars-active">
																					<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
																				</li>
																				<li style="display: none;">
																					<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
																				</li>
																			</ul>
																			<div style="display: none;" class="label-rating">132 reviews</div>
																			<div style="display: none;" class="label-rating">154 orders </div>
																		</div> <!-- rating-wrap.// -->
																</figcaption>
																<div class="bottom-wrap">
																	<a class="btn btn-sm btn-primary" href="?action=detalles&id=<?php echo $fila['id'];?>">Ver Detalles
																	</a>
																	<a href="carrito/VerCarrito.php?action=addToCart&id=<?php echo $fila["id"]; ?>" class="btn btn-sm btn-primary float-right">Añadir al carrito</a>
																	<div class="price-wrap h5">
																		<span class="price-new">$<?php echo $fila['product_price']; ?><!--</span> <del class="price-old">$1980</del>
																	</div>--> <!-- price-wrap.// -->
																	</div> <!-- bottom-wrap.// -->
															</figure>
														</div> <!-- col // -->
															<!-- productos EncontradosInicio -->
																<?php
															}
														}else{
									?>
										<div class="container">
											<div class="row">
												<div class="col-12">
													<center>
														<p style="background-color: orange;">
															<?php echo "No tenemos resultados con su busqueda pero..."; ?>
													</center>
												</div>
											</div>
											<div class="card">
												<article class="card-body mx-auto" style="max-width: 400px;">
													<a class="navbar-brand" href="#">
													<!--	<img style="width: 100%;" class="logo" src="../../img/flowRoot3336-4-2-6.png" alt="Home&Office" title="Home&Office">-->
													</a>
													<h4 class="card-title mt-3 text-center">Contáctanos</h4>
													<p class="text-center">!Fabricamos el mueble de tus sueños a tu medida!</p>

													<form action="fabricando.php" method="POST" >
													<div class="form-group input-group">
														<div class="input-group-prepend">
															<span class="input-group-text"> <i class="fa fa-user"></i> </span>
															</div>
														<input name="name" required="" class="form-control" placeholder="Nombre completo" type="text">
													</div> <!-- form-group// -->
													<div class="form-group input-group">
														<div class="input-group-prepend">
															<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
															</div>
														<input name="email" required="" class="form-control" placeholder="Email" type="email">
													</div> <!-- form-group// -->
													<div class="form-group input-group">
														<div class="input-group-prepend">
															<span class="input-group-text"> <i class="fa fa-phone"></i> </span>
														</div>
														<input name="phone" required="" class="form-control" placeholder="Número de teléfono" type="phone">
													</div> <!-- form-group// -->
													<div class="form-group input-group">
														<div class="input-group-prepend">
															<span class="input-group-text"> <i class="fa fa-building"></i> </span>
														</div>
														<select required="" name="profesion" class="form-control">
															<option selected=""> Ocupación</option>
															<option>Diseñador</option>
															<option>Gerente</option>
															<option>Ingenieria</option>
															<option>Otro</option>
														</select>
													</div> <!-- form-group end.// -->
													<div class="form-group input-group">
														<div class="input-group-prepend">
															<span class="input-group-text"> <i class="fa fa-comment-alt"></i> </span>
														</div>
															<textarea required="" name="text" class="form-control" placeholder="Mensaje (Detalles del producto que te gustaría tener por ejemplo.)" type="textarea"></textarea>
													</div> <!-- form-group// -->
													<div class="form-group">
														<button type="submit" class="btn btn-primary btn-block"> Enviar </button>
													</div> <!-- form-group// -->
												</form>
												</article>
											</div> <!-- card.// -->
										</div>
									<?php
											}
										}
									?>
							</div>
					</div>
	<!-- productos end-->
	<!-- =================BUSQUEDA ================= -->
	<!-- ================= main slide ================= -->
	<!-- ============== main slidesow .end // ============= -->
			<aside class="col-md-3">

			</aside>

<!--
	<figure class="mt-3 banner p-3 bg-secondary">
			<div class="text-lg text-center white">Useful banner can be here</div>
	</figure>
-->
	</div> <!-- container .//  -->
	</section>
	<!-- ========================= SECTION MAIN END// ========================= -->
	<!-- ========================= SECTION ITEMS ========================= -->
	<section class="section-request bg padding-y-sm">
			<div class="container">
					<header class="section-heading heading-line">
							<h4 class="title-section bg text-uppercase">Ofertas</h4>
					</header>

					<div class="row-sm">
							<?php
							$query = "SELECT * FROM productos WHERE product_category = 'ofertas' OR category_two = 'ofertas' ORDER BY 'product_price' DESC LIMIT 10";
							$resultado = $conexion->query($query);

							while ($row = $resultado->fetch_assoc()) {
							?>
									<div class="col-md-2">
											<figure class="card card-product">
													<div class="img-wrap"> <a href="productos/categorias/producto.php?id=<?php echo $row['id'];?>"><img src="data:image/jpg;base64,<?php echo base64_encode($row['product_img']);?>"></a></div>
													<figcaption class="info-wrap">
															<h6 class="title "><a href="productos/categorias/producto.php?id=<?php echo $row['id'];?>"><?php echo $row['product_name']; ?></a></h6>

															<div class="price-wrap">
																	<span class="price-new">$<?php echo $row['product_price']; ?></span><br>
																	<!--<del class="price-old">$1980</del>-->
																	<a href="productos/categorias/producto.php?id=<?php echo $row['id'];?>"><button type="button" class="btn btn-primary">Ver producto</button></a>
															</div> <!-- price-wrap.// -->

													</figcaption>
											</figure> <!-- card // -->
									</div> <!-- col // -->
							<?php
									}
							?>

					</div> <!-- row.// -->
			</div><!-- container // -->
	</section>

</div>
	<!-- ========================= SECTION ITEMS .END// ========================= -->

	<!-- ========================= SECTION LINKS ========================= -->
	<section class="section-links bg padding-y-sm">
	<div class="container">
	<div class="row">
	<!--
			<div class="col-sm-6">
	<header class="section-heading heading-line">
			<h4 class="title-section bg text-uppercase">Suppliers by Region</h4>
	</header>

	<ul class="list-icon row">
			<li class="col-md-4"><a href="#"><img src="images/icons/flag-usa.png"><span>United States</span></a></li>
			<li class="col-md-4"><a href="#"><img src="images/icons/flag-in.png"><span>India</span></a></li>
			<li class="col-md-4"><a href="#"><img src="images/icons/flag-tr.png"><span>Turkey</span></a></li>
			<li class="col-md-4"><a href="#"><img src="images/icons/flag-kr.png"><span>Korea</span></a></li>
			<li class="col-md-4"><a href="#"><img src="images/icons/flag-tr.png"><span>Turkey</span></a></li>
			<li class="col-md-4"><a href="#"><img src="images/icons/flag-kr.png"><span>Korea</span></a></li>
	</ul>
			</div>

			<div class="col-sm-6">
	<header class="section-heading heading-line">
			<h4 class="title-section bg text-uppercase">Trade services</h4>
	</header>
	<ul class="list-icon row">
			<li class="col-md-4"><a href="#"><i class="icon fa fa-comment"></i><span>Trade Assistance</span></a></li>
			<li class="col-md-4"><a href="#"><i class="icon  fa fa-suitcase"></i><span>Business Identity</span></a></li>
			<li class="col-md-4"><a href="#"><i class="icon fa fa-globe"></i><span>Worldwide delivery</span></a></li>
			<li class="col-md-4"><a href="#"><i class="icon fa fa-phone-square"></i><span>Customer support</span></a></li>
			<li class="col-md-4"><a href="#"><i class="icon fa fa-globe"></i><span>Worldwide delivery</span></a></li>
			<li class="col-md-4"><a href="#"><i class="icon fa fa-phone-square"></i><span>Customer support</span></a></li>
	</ul>
			</div>
			col // -->
	</div>
<!--
	<figure class="mt-3 banner p-3 bg-secondary">
			<div class="text-lg text-center white">Another banner can be here</div>
	</figure>
-->
	</div>
	</section>
	<!-- ========================= SECTION LINKS END.// ========================= -->

	<!-- ========================= SECTION SUBSCRIBE ========================= -->
<!--
	<section class="section-subscribe bg-secondary padding-y-lg">
	<div class="container">
	<p class="pb-2 text-center white">Delivering the latest product trends and industry news straight to your inbox</p>

	<div class="row justify-content-md-center">
			<div class="col-lg-4 col-sm-6">
	<form class="row-sm form-noborder">
					<div class="col-8">
					<input class="form-control" placeholder="Your Email" type="email">
					</div>
					<div class="col-4">
					<button type="submit" class="btn btn-block btn-warning"> <i class="fa fa-envelope"></i> Subscribe </button>
					</div>  col.//
	</form>
	<small class="form-text text-white-50">We’ll never share your email address with a third-party. </small>
			</div> col-md-6.
	</div>
					col.//


	</div>  container //
	</section>-->
	<!-- ========================= SECTION SUBSCRIBE END.//========================= -->
