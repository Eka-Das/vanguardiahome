<?php include("modal.php"); ?>
<section class="section-main bg padding-y-sm" style="background-color: black;">
	<div class="container">
		<div class="card">
			<div class="card-body">
				<div class="row row-sm">
					<aside class="col-md-3">
					</aside>
					<div class="col-md-6">
						<?php include("slideshow.php"); ?>
						<aside class="col-md-3">

						</aside>
					</div>
				</div> <!-- card-body .// -->
			</div> <!-- card.// -->
			<!--
				<figure class="mt-3 banner p-3 bg-secondary">
					<div class="text-lg text-center white">Useful banner can be here</div>
				</figure>
			-->
	</div> <!-- container .//  -->
</section>
	<!-- ========================= SECTION MAIN END// ========================= -->
	<!-- ========================= SECTION ITEMS ========================= -->
		<section class="section-request bg padding-y-sm">
			<div class="container">
				<header class="section-heading heading-line">
					<h4 class="title-section bg text-uppercase">Ofertas</h4>
				</header>

				<div class="row-sm">
					<?php
    					$query ="   SELECT * FROM productos
                                    WHERE product_category = 'ofertas'
                                    OR category_two = 'ofertas'
                                    ORDER BY id = 'id' DESC LIMIT 18";
    					$resultado = $conexion->query($query);

    					while ($row = $resultado->fetch_assoc()) {
					?>
						<div class="col-md-2">
							<figure class="card card-product">
								<div class="img-wrap">
									<a href="?action=detalles&id=<?php echo $row['id'];?>"><img src="data:image/jpg;base64,<?php echo base64_encode($row['product_img']);?>"></a>
								</div>
								<figcaption class="info-wrap">
									<h6 class="title" style="text-transform: uppercase;"><a href="?action=detalles&id=<?php echo $row['id'];?>"><?php echo $row['product_name']; ?></a></h6>

									<div class="price-wrap">
										<span class="price-new">$<?php echo $row['product_price']; ?></span><br>
										<!--<del class="price-old">$1980</del>-->
										<a href="?action=detalles&id=<?php echo $row['id'];?>"><button type="button" class="btn btn-primary">Ver producto</button></a>
									</div> <!-- price-wrap.// -->

								</figcaption>
							</figure> <!-- card // -->
						</div> <!-- col // -->
					<?php
						}
					?>

				</div> <!-- row.// -->
			</div><!-- container // -->
		</section>
