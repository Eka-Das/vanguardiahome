<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!--LogoNav-->
					<a class="navbar-brand mr-sm-2" href="?action=index">
						<img class="logo" src="images/logos/vanguardiaLogoNew2019FondoTransparente.png" alt="Vanguardia Home" title="Vanguardia Home">
					</a>
				<!--LogoNavEnd-->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categorías
        </a>
        <div class="dropdown-menu scroll bg-dark" aria-labelledby="navbarDropdown">
            <?php
                include ("administracion/login/conexion.php");

                $query = "SELECT * FROM categorias";
                $resultado = $conexion->query($query);

                while ($row = $resultado->fetch_assoc()) {?>
                    <a class="dropdown-item" href="?action=categoria&es=<?php echo $row['name'];?>"><button class="btn btn-dark" type="button"><?php echo $row['name'];?></button></a>
                    <?php
                }
            ?>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="blog/">Blog <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="?action=contacto">Contáctenos <span class="sr-only">(current)</span></a>
      </li>
      <li>
					<a class="nav-link" href="?action=catalogos">Catálogos</a>
      </li>
    </ul>
    <!--buscador-->
    
        <form action="buscando.php" class="py-1" method="GET" >
            <div class="input-group w-100">
                <input type="text" class="form-control search-dark" style="width:50%;" placeholder="Buscar" name="buscando">
                <div class="input-group-append">
                    <button class="btn btn-dark" type="submit" value="submit">
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </div>
            </div>
        </form>

    <!--buscadorFin-->

    <!--carritoIco-->
        <div class="col-lg-2 col-sm-12">
            <div class="widgets-wrap float-left row no-gutters py-1">
                <div class="col-auto">
                    <a href="?action=carrito" class="widget-header">
                        <div class="icontext">
                            <div class="icon-wrap"><i class="text-dark icon-sm fa fa-shopping-cart"></i></div>
                                <div class="text-wrap text-dark">
                                    Carrito <br> de compras
                                </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <!--carritoIcoFin-->
    
  </div>
</nav>