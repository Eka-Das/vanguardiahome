<?php session_start(); //$_SESSION['carrito']; ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("head.php"); ?>
    </head>
    <body>
			<!-- Google Tag Manager (noscript) -->
				<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T2FH2MP"
				height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->
			<!--fbsocialPlugin-->
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
						fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
				</script>
			<!--FbSocialPluginEnd -->
        
        <?php include("navbarTwo.php"); ?>
        <!-- MAIN SECTION -->
            <?php
                //EkaSwitch
                if (isset($_GET["action"])) {
                    $var=$_GET["action"];
                }else{
                    $var="index";
                }
                switch ($var) {

                    case 'index':
                        include("home.php");
                    break;

                    case 'categoria':
                        $tipo=$_GET['es'];
                        include("categorias.php");
                    break;

                    case 'buscador';
                        include("productos/busqueda/index.php");
                    break;

                    case 'detalles';
                        include("producto/detalles.php");
                    break;

                    case 'catalogos';
                        include('catalogo.php');
                    break;

                    case 'ver';
                        include('administracion/subirpdf/ver.php');
                    break;

                    case 'contacto';
                        include('contacto.php');
                    break;

                    case 'carrito':
                        include('carrito/VerCarrito.php');
                    break;

                    case 'confirmarPedido':
                        include('carrito/registrate.php');
                    break;

                    case 'demo';
                        include('demo.php');
                    default:
                }
            ?>
        <!-- SECTION MAIN END -->

        <?php include ("footer.php");?>
    </body>

</html>
