<!--NavIn-->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
		<button class="navbar-toggler"
				type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
		</button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
				<!--LogoNav-->
					<a class="navbar-brand" href="?action=index">
						<img class="logo" src="images/logos/vanguardiaLogoNew2019FondoTransparente.png" alt="Vanguardia Home" title="Vanguardia Home">
					</a>
				<!--LogoNavEnd-->

				<ul class="navbar-nav">
					<div class="dropdown">
						<button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Categorías
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
							<?php
								include ("administracion/login/conexion.php");

								$query = "SELECT * FROM categorias";
								$resultado = $conexion->query($query);

								while ($row = $resultado->fetch_assoc()) {?>
									<a class="dropdown-item" href="?action=categoria&es=<?php echo $row['name'];?>"><button class="btn btn-dark" type="button"><?php echo $row['name'];?></button></a>
									<?php
								}
							?>
						</div>
					</div>
						<!--<a class="dropdown-item" href="?action=catalogos"><button class="btn btn-dark" type="button">Catálogos</button></a>-->
						<a class="dropdown-item" href="?action=contacto"><button class="btn btn-dark" type="button">Contáctanos</button></a>
						<a class="dropdown-item" href="blog/"><button class="btn btn-dark" type="button">Blog</button></a>
				</ul>

				<!--buscador-->
					<div class="col-lg-6-24 col-sm-8">
						<form action="buscando.php" class="py-1" method="GET" >
							<div class="input-group w-100">
								<input type="text" class="form-control" style="width:50%;" placeholder="Buscar" name="buscando">
								<div class="input-group-append">
									<button class="btn btn-default" type="submit" value="submit">
										<i class="fa fa-search"></i> Buscar
									</button>
								</div>
							</div>
						</form>
					</div>
				<!--buscadorFin-->

				<!--carritoIco-->
					<div class="col-lg-2 col-sm-12">
						<div class="widgets-wrap float-right row no-gutters py-1">
							<div class="col-auto">
								<a href="?action=carrito" class="widget-header">
									<div class="icontext">
										<div class="icon-wrap"><i class="text-dark icon-sm fa fa-shopping-cart"></i></div>
											<div class="text-wrap text-dark">
												Carrito <br> de compras
											</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				<!--carritoIcoFin-->
							</div>
		</div>
    </nav>
<!--NavEnd-->
