<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content=" Bootstrap-ecommerce by Vosidiy-Edited by Eka from PcLabs.tech">

<title>Vanguardia Home</title>

<link rel="shortcut icon" type="image/x-icon" href="images/Hao.png">

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Bootstrap4 files-->
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

<!-- plugin: fancybox  -->
<script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
<link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

<!-- plugin: owl carousel  -->
<link href="plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
<script src="plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- custom style -->
<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

<!-- custom javascript -->
<script src="js/script.js" type="text/javascript"></script>
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T2FH2MP');</script>
<!-- End Google Tag Manager -->

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-6028288480509132",
          enable_page_level_ads: true
     });
</script>
<style>
            .bg-dark{
                background-color: black !important;
            }
            .section-header{
                background-color:black;
            }
            .text-dark {
                color: #b27e00 !important;
            }
            .btn-dark {
                color: #b27e00;
                background-color: #000;
                border-color: gold;
            }
            .dropdown-item:hover {
                color: #b27e00;
                background-color: #000;
                border-color: #000;
            }
            .btn-default{
                background-color: black;
                color: #b27e00;
            }
            .scroll{
                height: 300px;
                overflow-y: scroll;
            }
            .search-dark{
                border-color: gold;
            }
            .nav-link{
                color: gold !important;
            }
        </style>