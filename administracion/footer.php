<!-- FOOTER -->
    <footer class="section-footer bg-dark" style="margin-top: 1rem;">
        <div class="container">
            <section class="footer-top padding-top">
                <div class="row">
                    <aside class="col-sm-3  col-md-3 white"><!--
                        <h5 class="title">My Account</h5>
                        <ul class="list-unstyled">
                            <li> <a href="#"> User Login </a></li>
                            <li> <a href="#"> User register </a></li>
                            <li> <a href="#"> Account Setting </a></li>
                            <li> <a href="#"> My Orders </a></li>
                            <li> <a href="#"> My Wishlist </a></li>
                        </ul>
                        -->
                        <center>
                            <b>
                            Teléfonos<br>
                            </b>
                            <p>
                                <b>Jalisco</b><br>
                                <b>Matríz: </b>(01)33 2687 2497<br>
                                (01)33 2733 6074<br>
                                <b>WhatsApp: </b>33 1834 5733<br>
                            </p>
                        </center>
                    </aside>
                    <aside class="col-sm-3  col-md-3 white">
                        <strong>
                            Contáctanos:
                        </strong>
                        <p>Bahía de todos los santos 2889<br>Col. Parques de Sta. María <br>Tlaquepaque Jalisco México</p>
                    </aside>
                    <aside class="col-sm-3">
                        <article class="white">
                            <h5 class="title">Contactos</h5>
                            <p>
                                <strong>Matríz: </strong>33 2733 6074 <br>
                                <strong>Bodega: </strong>33 2687 2497 <br>
                                <strong>WhatsApp:</strong>  33 1834 5733
                            </p>
                        </article>
                    </aside>
                    <aside class="col-sm-3 col-md-3 white">
                        <!--fbSocialPluginsButtons-->
                        <div class="btn-group white">
                            <a class="btn btn-instagram" title="Instagram" target="_blank" href="https://www.instagram.com/homeandhoffice.store/"><i class="fab fa-instagram  fa-fw"></i></a>
                            <a class="btn btn-facebook" title="Facebook" target="_blank" href="https://www.facebook.com/HAOStoreMx/"><i class="fab fa-facebook-f  fa-fw"></i></a><!--
                            <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
                            <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>-->
                        </div>
                        <br><br>
                        <div style="text-align: center; color: white;" class="fb-like" data-href="https://www.facebook.com/HAOStoreMx/" data-width="100" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true">
                        </div>
                    </aside>
                </div> <!-- row.// -->
                <br>
            </section>
            <section class="footer-bottom row border-top-white">
                <div class="col-sm-6">
                    <!--
                    <p class="text-white-50"> Made with <3 <br>  by Vosidiy M.</p>
                    -->
                </div>
                <div class="col-sm-6">
                    <p class="text-md-right text-white-50">
                        Copyright &copy  <br><!--
                        <a href="http://bootstrap-ecommerce.com" class="text-white-50">Bootstrap-ecommerce UI kit</a>-->
                    </p>
                </div>
            </section> <!-- //footer-top -->
        </div><!-- //container -->
    </footer>
<!-- FOOTER END -->
</body>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5b7ee0f5afc2c34e96e7d6ab/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
