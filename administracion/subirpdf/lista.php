<table class="table">
    <thead>
        <tr>
            <th scope="col">Título</th>
            <th scope="col">Descripcion</th>
            <th scope="col">Tamaño</th>
            <th scope="col">Tipo</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
            include 'config.inc.php';
            $db=new Conect_MySql();
            $sql = "SELECT*FROM tbl_documentos";
            $query = $db->execute($sql);
            while($datos=$db->fetch_row($query)){
            ?>
                <tr>
                    <td><?php echo $datos['titulo']; ?></td>
                    <td><?php echo $datos['descripcion']; ?></td>
                    <td><?php echo $datos['tamanio']; ?></td>
                    <td><?php echo $datos['tipo']; ?></td>
                    <td>
                        <a href="?action=ver_archivo&id=<?php echo $datos['id_documento']?>">
                            <?php echo $datos['nombre_archivo']; ?>
                        </a>
                    </td>
                    <td>
                        <a href="editar.php">
                            <i class="fas fa-fw fa-edit"></i>
                        </a>
                        <a href="?action=eliminar_archivo&id=<?php echo $datos['id_documento']?>&archivo=<?php echo $datos['nombre_archivo']; ?>">
                            <i class="fas fa-fw fa-trash"></i>
                        </a>                    
                    </td>
                </tr>
        <?php  
            } 
        ?>    
    </tbody>
</table>