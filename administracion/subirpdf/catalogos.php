<table class="table">
    <thead>
        <tr>
            <th scope="col">Título</th>
            <th scope="col">Descripcion</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
            include 'config.inc.php';
            $db=new Conect_MySql();
                $sql = "SELECT*FROM tbl_documentos";
                $query = $db->execute($sql);
                while($datos=$db->fetch_row($query)){
                ?>
                    <tr>
                        <td><?php echo $datos['titulo']; ?></td>
                        <td><?php echo $datos['descripcion']; ?></td>
                        <td>
                            <a href="?action=ver&id=<?php echo $datos['id_documento']?>"><i class="fas fa-fw fa-eye"></i>Ver</a>
                        </td>
                        <td>
                            <a href="edliminar"><i class="fas fa-fw fa-download"></i>Descargar</a>                    
                        </td>
                    </tr>
        <?php  
            } 
        ?>    
    </tbody>
</table>