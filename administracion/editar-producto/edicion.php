<?php
include("./login/conexion.php");

	$id = $_REQUEST['id'];
	$query = "SELECT * FROM productos WHERE id ='$id'";
	$resultado = $conexion->query($query);
	$row = $resultado->fetch_assoc();
?>
<div class="row">
	<form class="col-md-12 offset-md-3" action="editar-producto/modificando.php?id=<?php echo $row['id'];?>" enctype="multipart/form-data" method="POST">
		<!--nombre -->
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="product_name">Nombre del producto:</label> <br>
				<input class="form-control" type="text" required name="product_name" value="<?php echo $row['product_name']; ?>">
			</div>
		</div>
		<!--Categoría-->
		<div class="form-row">
			<div class="form-group col-md-6">
				<label>Categoría</label><br>
				<input class="form-control" type="text" name="product_category" required="" value="<?php echo $row['product_category']; ?>">
			</div>
		</div>
		<!--precio-->
		<div class="form-row">
			<div class="form-group col-md-3">
				<label for="precio2"><strong>$</strong>Precio Tachado</label>
				<input class="form-control" type="text" name="price_two" value="<?php echo $row['price_two']; ?>">
			</div>
			<div class="form-group col-md-3">
				<label><strong>$</strong>Precio1</label><br>
				<input class="form-control" type="text" name="product_price" required="" placeholder="$" value="<?php echo $row['product_price']; ?>">
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-6">
				<!--Descripción-->
				<label>Descripción</label><br>
				<textarea class="form-control" name="product_description" rows="3" cols="30"><?php echo $row['product_description']; ?></textarea>
			</div>
		</div>
		<!--imagen -->
		<div class="form-row">
			<div class="form-group col-md-3">
				<label for="product_img">Imágen del producto</label><br>
				<img height="120px" src="data:image/jpg;base64,<?php echo base64_encode($row['product_img']); ?>"/><br>
				<a class="btn btn-primary" href="eliminarimg.php?id=<?php echo $row['id'];?>">Borrar Imagen</a>
			</div>
			<div class="form-group col-md-3">
				<label>Cambiar Imagen</label>
				<input class="form-control-file" name="product_img" type="file" value="0" />
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-6">
				<input class="form-control btn btn-success" type="submit" value="Subir producto" />
			</div>
		</div>
	</form>
</div>

