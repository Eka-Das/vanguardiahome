<div class="container">
	<div class="row">
		<div class="col-md-12">
			<center>
				<h1>Editar producto</h1>
				<p>Selecciona la categoría del producto que quieres editar</p>
			</center>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4">
			<ul class="list-group">
				<center>
					<?php

						$consulta = 'SELECT * FROM categorias';
						$resultado = $conexion->query($consulta);

						if ($resultado) {
							while ($fila = $resultado->fetch_assoc()) {?>
        						<li class="list-group-item">
        							<a href="?action=categoria&categoria=<?php echo $fila['name'];?>">
        								<?php echo $fila['name'];?>
        							</a>
        						</li>
        					<?php
		                  }
						}else{
					?>
						<p class="bg-danger text-white">
							<?php echo 'No hay categorías';?>
						</p>
					<?php
						}
					?>
				</center>
			</ul>
		</div>
		<div class="col-md-4">
		</div>
	</div>
</div>
