<?php
	include("./login/conexionlogin.php");
	if(isset($_SESSION['login'])){
?>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<center>
					<div class="title">
						<h1>Añadir un nuevo producto</h1>
						<hr>
					</div>
					<form class="align-middle" action="subir-producto/subiendo.php" enctype="multipart/form-data" method="POST">
							<!--nombre -->
						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="product_img">Nombre del producto:</label>
								<input class="form-control" type="text" required="" name="product_name">
							</div>
						</div>
							<!--Descripción-->
						<div class="form-row">
							<div class="form-group col-md-12">
								<label>Descripción</label><br>
								<textarea class="form-control" name="product_description" rows="2" cols="30"></textarea>
							</div>
							<div class="form-group col-md-6"></div>
						</div>
							<!--precio-->
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Precio Anterior Ejemplo: <del>$2500</del></label>
								<input class="form-control" paceholder="$" name="price_two" required="">
							</div>
							<div class="form-group col-md-6">
								<label>Precio Nuevo</label><br>
								<input class="form-control" type="number" name="product_price" required="" placeholder="$">
							</div>
						</div>
						<div class="form-row">
							<!--Categorías-->
							<div class="form-group col-md-6">
								<label>Categoría</label>
								<select class="form-control" name="product_category" required="";>
									<?php
										include '../login/conexion.php';

										$consulta = 'SELECT * FROM categorias';
										$resultado = $conexion->query($consulta);

										if ($resultado) {

											while ($fila = $resultado->fetch_assoc()) {?>
        										<option href="switch.php?categoria=<?php echo $fila['name'];?>">
        												<?php echo $fila['name'];?>
        										</option>
				                               <?php
    										}
    									}else{
									?>
										<p class="bg-danger text-white"><?php echo 'No hay categorías';?></p>
									<?php
											}
									?>
								</select>
							</div>
							<!--imagen -->
							<div class="form-group col-md-6">
								<label for="product_img">Imágen del producto</label>
								<input class="form-control-file" name="product_img" type="file" /><br>
							</div>
						</div>
						<div class="form-group">
							<input class="form-control btn-success" type="submit" value="Subir producto" />
						</div>
					</form>
				</center>
			</div>
		<div class="col-md-3"></div>
	</div>
<?php
    }else {
		header ('Location: ../login/index.php');
	}
	?>
