<!DOCTYPE html>
<html>
<head>
	<title>Productos</title>
	<!-- Online --> 
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<!--local-->
		<link rel="stylesheet" href="/media/e-k/HDD/HomeAndOffice/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="/media/e-k/HDD/HomeAndOffice/dist/css/bootstrap.css">
		<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<script src="/media/e-k/HDD/HomeAndOffice/dist/jquery/jquery.min.js"></script>
</head>
<body>
	<?php
			session_start();

			if(isset($_SESSION['login'])){
	?>
        <div class="container-flui">
        	<div class="row">
        		<div col-md-12>
        			<p align="center">
        				<a href="index.php">
        					<img src="../../../img/flowRoot3336-4-2-6.png" width="300px" >
        				</a>
        			</p>
        		</div>
        	</div>
        </div>
	<center>
		<table>
			<thead>
				<tr>
					<th>imagen</th>
					<th>operaciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					include("../../login/conexion.php");

					$query = "SELECT * FROM presentacion";
					$resultado = $conexion->query($query);

					while ($row = $resultado->fetch_assoc()) {
				?> 
						<tr>
							<td>
								<img height="120px" src="data:image/jpg;base64,<?php echo base64_encode($row['img_2']);?>"/>
							</td>
							<th>
								<a href="editar_img_2.php?id=<?php echo $row['id'];?>">Editar</a>
							</th>
						</tr>
			</tbody>
		</table>
		<br>
		<a href="../index.php">
			<button>Regresar</button>
		</a>
	</center>
		<?php
			}
				
		}else{
				header('Location: ../login/index.php');
			}
		?>
</body>
</html>