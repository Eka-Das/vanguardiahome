<!DOCTYPE html>
<html>
	<head>
		<title>Productos</title>
		<!-- Online --> 
			<meta charset="utf-8">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				<!--local-->
			<link rel="stylesheet" href="/media/e-k/HDD/HomeAndOffice/dist/css/bootstrap.min.css">
			<link rel="stylesheet" href="/media/e-k/HDD/HomeAndOffice/dist/css/bootstrap.css">
			<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
			<script src="/media/e-k/HDD/HomeAndOffice/dist/jquery/jquery.min.js"></script>
	</head>
	<body>
		<?php
			session_start();

			if(isset($_SESSION['login'])){
		?>
			<!--img_head-->
		    <div>
		    	<center>
			    	<a href="../../index.php">
			    		<img src="../../img/flowRoot3336-4-2-6.png" width="300px" >
			    	</a>
			    </center>
		    </div>
				<center>
					<script type="text/javascript">
						function button(idButton){
							var formulario = document.getElementById('formulario');

							switch(idButton){

								case 1:
									formulario.style.display = 'block';
							}
						}
					</script>
						<hr>
						<h4>Catálogos</h4>
					<a onclick="button(1)"><button>SubirCatalogo</button></a>
					<hr>
					<div id="formulario" style="display: none;">
						<form action="subiendo.php" method="post" enctype="multipart/form-data">
						    Select image to upload:
						    <input type="text" name="titulo">
						    <input type="file" name="userfile" id="fileToUpload">
						    <input type="submit" value="Upload Image" name="submit">
						</form>
						<hr>
					</div>
		    <!--tabla de imagen-->
			<?php
				include("../login/conexion.php");

				$query = "SELECT * FROM CATALOGOS";
				$resultado = $conexion->query($query);

				while ($row = $resultado->fetch_assoc()) {
			?> 
					<table>
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Link de descarga</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<!--<img height="120px" src="data:image/jpg;base64,<?php echo base64_encode($row['img_1']);?>"/>-->
									<?php ($row['NOMBRE']);?>
								</td>
								<th>
									<a href="editar_img_1.php?id=<?php echo $row['id'];?>">Editar</a>
								</th>
							</tr>
						</tbody>
					</table>
					<br>
					<a href="../index.php"><button>Regresar</button></a>
				</center>
		<?php
			}

		}else{
				header('Location: ../login/index.php');
			}
		?>
	</body>
</html>