<?php
   include("login/conexionlogin.php");
	session_start();
	if(isset($_SESSION['login'])){
?>
	<!DOCTYPE html>
	<html>
		<head>
			<title>Areas de administracion</title>
			<meta charset="utf-8">
			<link rel="stylesheet" type="text/css" href="../dist/css/haostyle.css">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Bootstrap core CSS-->
            <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

            <!-- Custom fonts for this template-->
            <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

            <!-- Page level plugin CSS-->
            <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

            <!-- Custom styles for this template-->
            <link href="css/sb-admin.css" rel="stylesheet">

		</head>
		<body>
            <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="index.php">
            <img src="../img/VanguardiaHomeLogo.png" width="200px">
        </a>
        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>

        <!-- Navbar Search -->
        <form   class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"
                method="GET" action="buscando.php">
            <div class="input-group">
                <input type="text" class="form-control"
                    placeholder="Buscar . . ." aria-label="Search"
                    aria-describedby="basic-addon2" name="buscando">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
          <!-- Navbar -->
          <ul class="navbar-nav ml-auto ml-md-0">
              <li class="nav-item dropdown no-arrow">
                  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user-circle fa-fw"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown"><!--
                  <a class="dropdown-item" href="#">Settings</a>
                  <a class="dropdown-item" href="#">Activity Log</a>
                  <div class="dropdown-divider"></div>-->
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Cerrar Sesión</a>
                  </div>
              </li>
          </ul>
      </nav>

      <div id="wrapper">
        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <!--
                <li class="nav-item active">
                    <a class="nav-link" href="index.html">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            -->
            <!--Presentación -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-image"></i>
                    <span>Presentación</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <h6 class="dropdown-header">Presentación</h6>
                    <a class="dropdown-item" href="?action=subir_img_presentacion">Añadir imágen</a>
                    <a class="dropdown-item" href="?action=nueva_imagen">Cambiar imágen</a>
                    <a class="dropdown-item" href="?action=nueva_imagen">Editar</a>
                    <a class="dropdown-item" href="?action=eliminar_imagen">Eliminar</a><!--
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">Other Pages:</h6>
                    <a class="dropdown-item" href="404.html">404 Page</a>
                    <a class="dropdown-item" href="blank.html">Blank Page</a>-->
                </div>
            </li>

            <!--Categorías-->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-paw"></i>
                    <span>Categorías</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <h6 class="dropdown-header">Categorías</h6>
                    <a class="dropdown-item" href="?action=anadir_categoria">Nuevo</a>
                    <!--<a class="dropdown-item" href="editar-producto/index.php">Editar</a>-->
                    <a class="dropdown-item" href="?action=eliminar_categoria">Eliminar</a><!--
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">Categorías</h6>
                    <a class="dropdown-item" href="administracion/categorias/">Editar</a>
                    <a class="dropdown-item" href="blank.html">Blank Page</a>-->
                </div>
            </li>
            <!--Productos-->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-tree"></i>
                        <span>Productos</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <h6 class="dropdown-header">Productos</h6>
                        <a class="dropdown-item" href="?action=anadir_producto">Nuevo</a>
                        <a class="dropdown-item" href="?action=editar_producto">Editar</a>
                        <a class="dropdown-item" href="?action=editar_producto">Eliminar</a><!--
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Categorías</h6>
                        <a class="dropdown-item" href="administracion/categorias/">Editar</a>
                        <a class="dropdown-item" href="blank.html">Blank Page</a>-->
                    </div>
                </li>
            <!--Productos-->

            <!--Catálogos-->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-file"></i>
                    <span>Catálogos</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <h6 class="dropdown-header">Catálogos</h6>
                    <a class="dropdown-item" href="?action=ver_catalogos">Ver</a>
                    <a class="dropdown-item" href="?action=subir_catalogo">
                        <i class="fas fa-fw fa-upload" aria-hidden="true">
                        </i> Subir catálogo
                    </a>
                    <!--<a class="dropdown-item" href="?action=editar_producto">Eliminar</a>--><!--
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">Categorías</h6>
                    <a class="dropdown-item" href="administracion/categorias/">Editar</a>
                    <a class="dropdown-item" href="blank.html">Blank Page</a>-->
                </div>
            </li>
            <!--Catálogos-->

            <!--Detalles-->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-file"></i>
                        <span>Detalles</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <h6 class="dropdown-header">Detalles</h6>
                        <!--<a class="dropdown-item" href="?action=detalles">Cambiar Información<br> de la página</a>
                        <a class="dropdown-item" href="?action=subir_catalogo">
                            <i class="fas fa-fw fa-upload" aria-hidden="true">
                            </i> Subir catálogo
                        </a>
                        <a class="dropdown-item" href="?action=editar_producto">Eliminar</a>--><!--
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Categorías</h6>
                        <a class="dropdown-item" href="administracion/categorias/">Editar</a>
                        <a class="dropdown-item" href="blank.html">Blank Page</a>-->
                    </div>
                </li>
            <!--Detalles-->


            <!--
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Pages</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <h6 class="dropdown-header">Login Screens:</h6>
                        <a class="dropdown-item" href="login.html">Login</a>
                        <a class="dropdown-item" href="register.html">Register</a>
                        <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Other Pages:</h6>
                        <a class="dropdown-item" href="404.html">404 Page</a>
                        <a class="dropdown-item" href="blank.html">Blank Page</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="charts.html">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Charts</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="tables.html">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Tables</span>
                    </a>
                </li>
            -->
        </ul>
          <div id="content-wrapper">
            <div class="container-fluid">
              <?php
                if(isset($_GET["action"])){
                    $var = $_GET["action"];
                  }else{
                    $var = "index";
                  }
                  switch ($var) {
                    /*Index*/
                      case 'index':
                          include("home.php");
                      break;

                    //Productos In
                      case 'anadir_producto':
                          include("subir-producto/index.php");
                      break;

                      case 'editar_producto':
                          include("editar-producto/index.php");
                      break;

                      case 'categoria': //categoría seleccionada(para elegir el producto).
                          include('editar-producto/switch.php');
                      break;

                      case 'editar':
                          include('editar-producto/edicion.php');
                      break;

                      case'modificando':
                          include('editar-producto/modificando.php');
                      break;

                      case 'eliminar':
                          include('editar-producto/eliminar.php');
                      break;
                  //Productos End

                  /*categorías*/
                      case 'anadir_categoria':
                          include("categorias/index.php");
                      break;

                      case 'eliminar_categoria':
                          include("categorias/index.php");
                      break;
                  /*categorías*/

                  /*Presentación de inicio*/
                      case 'subir_img_presentacion':
                          include("presentacion/subir.php");
                      break;

                      case 'editar_imagen_presentacion':
                          include("presentacion/index.php");
                      break;
                  /*Presentación de inicio End*/

                      break;
                  /* Catálogos*/
                      case 'subir_catalogo':
                          include("subirpdf/index.php");
                      break;

                      case 'ver_catalogos':
                          include("subirpdf/lista.php");
                      break;

                      case 'ver_archivo';
                          include("subirpdf/archivo.php");
                      break;

                      case 'eliminar_archivo';
                          include("subirpdf/archivos/eliminar.php");
                      break;
                  /* Catálogos*/
                  /* Detalles In*/
                        case 'detalles':
                            include("detalles/index.php");
                        break;
                  /* Detalles End*/
                        case 'buscador':
                            include('editar-producto/buscareditar.php');
                        break;



                  default:

              }

          ?>

                    <!-- /.container-fluid -->

                    <!-- Sticky Footer -->
                    <footer class="sticky-footer">
                        <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © <a href="http://aliendevelop.pleyades.store">Alien Develop</a> 2019</span>
                        </div>
                        </div>
                    </footer>

                </div>
                <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>

            <!-- Logout Modal-->
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">¿Seguro(a) que quiere salir?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Selecciona "Si" para Salir o Cancelar para quedarte. </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                            <a class="btn btn-primary" href="login/cerrar_sesion.php">Si</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Bootstrap core JavaScript-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Page level plugin JavaScript-->
            <script src="vendor/chart.js/Chart.min.js"></script>
            <script src="vendor/datatables/jquery.dataTables.js"></script>
            <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin.min.js"></script>

            <!-- Demo scripts for this page-->
            <script src="js/demo/datatables-demo.js"></script>
            <script src="js/demo/chart-area-demo.js"></script>

		</body>
	</html>
<?php
    }else{
    	header ('Location: login/index.php');
	}
?>
