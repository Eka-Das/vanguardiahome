<!-- FOOTER -->
    <footer class="section-footer bg-dark" style="margin-top: 1rem;">
        <div class="container">
            <section class="footer-top padding-top">
                <div class="row">
                    <aside class="col-sm-12  col-md-6 col-lg-5 white"><center>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.855769046529!2d-103.41651938551324!3d20.59394688623548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428ace666d203f5%3A0xc9346d7ec753b1ff!2sAv.+Bah%C3%ADa+de+Todos+Los+Santos+2889%2C+Parques+de+Santa+Mar%C3%ADa%2C+45609+San+Pedro+Tlaquepaque%2C+Jal.!5e0!3m2!1ses-419!2smx!4v1559084889311!5m2!1ses-419!2smx"
                                width="400" height="200" frameborder="0" style="border:0" allowfullscreen>
                        </iframe></center>
                    </aside>
                    <aside class="col-sm-4 col-md-3 col-lg-3 white">
                        <strong>
                            Dirección:
                        </strong>
                        <p>Bahía de todos los santos 2889 Int #34<br>
                            Col. Parques de Sta. María <br>
                            Tlaquepaque Jalisco México
                        </p>
                    </aside>
                    <aside class="col-sm-4  col-md-2 col-lg-2 white">
                        <center>
                            <b>Teléfono</b>
                            <p>
                                <b>Jalisco</b><br>
                                <b>Matríz: </b>(01)3316418670<br>
                            </p>
                        </center>
                    </aside>


                    <aside class="col-sm-4 col-md-3 col-lg-2 white">
                        <!--fbSocialPluginsButtons-->
                        <div class="btn-group white">
                            <center>
                                <a class="btn btn-instagram" title="Instagram" target="_blank" href="https://www.instagram.com/homeandhoffice.store/"><i class="fab fa-instagram  fa-fw"></i></a>
                                <a class="btn btn-facebook" title="Facebook" target="_blank" href="https://www.facebook.com/HAOStoreMx/"><i class="fab fa-facebook-f  fa-fw"></i></a><!--
                                <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
                                <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>-->
                            </center>
                        </div>
                        <div style="text-align: center; color: white;" class="fb-like" data-href="https://www.facebook.com/HAOStoreMx/" data-width="100" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true">
                        </div>
                    </aside>
                </div> <!-- row.// -->
                <br>
            </section>
            <section class="footer-bottom row border-top-white">
                <div class="col-sm-6">
                    <!--
                    <p class="text-white-50"> Made with <3 <br>  by Vosidiy M.</p>
                    -->
                </div>
                <div class="col-sm-6">
                    <p class="text-md-right text-white-50">
                        Copyright &copy  <br>
                        <a href="http://pleyades.store"
                            class="text-white-50">Alien Develop</a>
                    </p>
                </div>
            </section> <!-- //footer-top -->
        </div><!-- //container -->
    </footer>
<!-- FOOTER END -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5b7ee0f5afc2c34e96e7d6ab/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
