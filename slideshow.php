<style>
/* Make the image fully responsive */
.carousel-inner img {
    width: 100%;
    height: 100%;
}
</style>
</head>
<body>

<div id="demo" class="carousel slide" data-ride="carousel">

<!-- Indicators -->
<ul class="carousel-indicators">
  <li data-target="#demo" data-slide-to="0" class="active"></li>
  <li data-target="#demo" data-slide-to="1"></li>
  <li data-target="#demo" data-slide-to="2"></li>
</ul>

<!-- The slideshow -->
<div class="carousel-inner">
    <?php
        include ('administracion/login/conexion.php');
        $query_result = mysqli_query($conexion, "SELECT * FROM presentacion");

        for ($i = 0; $i < $query_result->num_rows; $i++) {

            $user = $query_result->fetch_array();

            if ($i==0) {

            ?>
                <div class="carousel-item active">
                    <img src="data:image/jpeg;base64,<?php echo base64_encode($user['img']);?>" width="1100" height="500"/>
                </div>
        <?php
            }
            if ($i>0) {?>

                <div class="carousel-item">
                    <img src="data:image/jpeg;base64,<?php echo base64_encode($user['img']);?>" width="1100" height="500"/>
                </div>

        <?php
            }

        }
    ?>
</div>

<!-- Left and right controls -->
<a class="carousel-control-prev" href="#demo" data-slide="prev">
  <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
  <span class="carousel-control-next-icon"></span>
</a>
</div>
