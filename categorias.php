<section class="section-request bg padding-y-sm">
	<div class="container">
		<header class="section-heading heading-line">
			<h4 class="title-section bg text-uppercase"><?php echo $tipo ?></h4>
		</header>
		<div class="row">
			<?php
				$perpage = 2;

				if(isset($_GET['page']) & !empty($_GET['page'])){
					$curpage = $_GET['page'];
				}else{
					$curpage = 1;
				}

				$query = "
					SELECT * FROM productos
					WHERE product_category = '$tipo'
					OR category_two ='$tipo'
					ORDER BY id = 'id' DESC
					";
				$resultado = $conexion->query($query);
				$rowcount=mysqli_num_rows($resultado);

				if($rowcount<=0){
					echo "No hay productos en esta categoría";
				}

				while ($row = $resultado->fetch_assoc()) {

						if($rowcount==0){
							echo "no hay productos en esta categoría";
						}
				?>
					<div class="col-md-3">
						<figure class="card card-product">
							<div class="img-wrap">
								<img src="data:image/jpg;base64,<?php echo base64_encode($row['product_img']);?>">
							</div>
							<figcaption class="info-wrap">
								<a style="text-transform: uppercase;" href="?action=detalles&id=<?php echo $row['id'];?>" class="title"><?php echo $row['product_name']; ?></a>
								<div class="action-wrap">
									<!--<a href="#" class="btn btn-primary btn-sm float-right"> Comprar </a>-->
									<a href="?action=carrito&id=<?php echo $row["id"];?>" class="btn btn-primary btn-sm float-right"> Añadir al carrito </a>
									<div class="price-wrap h5">
										<span class="price-new">$<?php echo $row['product_price']; ?></span>
											<!--<del class="price-old">$1980</del>-->
									</div> <!-- price-wrap.// -->
								</div> <!-- action-wrap -->
							</figcaption>
						</figure> <!-- card // -->
					</div> <!-- col // -->
			<?php
				}
			?>
		</div> <!-- row.// -->

	</div><!-- container // -->
	<!--pagination--><!--
		<div class="container">
			<div class="row">
				<?php
					$start = ($curpage * $perpage) - $perpage;

					$PageSql = "SELECT * FROM productos
					WHERE product_category = '$tipo'
					OR category_two ='$tipo'
					ORDER BY id = 'id' DESC LIMIT 0, 1";
					$pageres = mysqli_query($conexion, $PageSql);
					$totalres = mysqli_num_rows($pageres);

					$endpage = ceil($totalres/$perpage);
					$startpage = 1;
					$nextpage = $curpage + 1;
					$previouspage = $curpage - 1;
				?>

				<nav aria-label="Page navigation example">
					<ul class="pagination">
						<li class="page-item">
						<a class="page-link" href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
						<a class="page-link" href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>-->
	<!--pagination-->

</section>
