<!-- Details Start -->
    <?php
        include("./administracion/login/conexion.php");

        $id = $_REQUEST['id'];
        $query = "SELECT * FROM productos WHERE id ='$id'";
        $resultado = $conexion->query($query);
        $row = $resultado->fetch_assoc();
    ?>
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <header class="section-heading heading-line">
                <h4 class="title-section bg text-uppercase">Detalles</h4>
            </header><!---->
            <!--
            <div class="card">
                <div class="card-body">
                    <div class="row row-sm">
                        
                        <aside class="col-md-3">
                        </aside>
                        <div class="col-md-6">
                            <aside class="col-md-3">
                            </aside>
                        </div>
                    </div> 
                </div> 

                <figure class="mt-3 banner p-3 bg-secondary">
                    <div class="text-lg text-center white">Useful banner can be here</div>
                </figure>
                
            </div>
            -->
        </div>
    </section>
    <div class="container">
        <div class="row">
            <!--img container-->
            <div class="col-sm-3">
                <!-- Trigger the Modal -->
                <img id="myImg" src="data:image/jpg;base64,<?php echo base64_encode($row['product_img']);?>" alt="<?php echo $row['product_name'];?>" style="width:100%;max-width:300px">
                <!-- The Modal -->
                <div id="myModal" class="modal">
                    <!-- The Close Button -->
                    <span class="close">&times;</span>

                    <!-- Modal Content (The Image) -->
                    <img class="modal-content" id="img01">

                    <!-- Modal Caption (Image Text) -->
                    <div id="caption"></div>
                </div>
            </div>
            <!--divisor-->
            <div class="col-sm-3"></div>

            <!--Description-->
            <div class="col-sm-6">
                <!--Title-->
                <h1 style="text-transform: uppercase;"><strong><?php echo $row['product_name']; ?></strong></h1>
                <!--Description-->
                <h5>Descripción: </h5>
                <p><?php echo $row['product_description']; ?></p>
                <!--Price-->
                
                <?php if($row['price_two'] > 0){ ?>
                    <h6 style="float: left; color: gray;"><del>$<?php echo $row['price_two'];?></del></h6>
                <?php
                    }
                ?>
                <h3 style="float: left;">$<?php echo $row['product_price'];?></h3>
                <!--Buttons-->
                <div style="padding-top: 61px;">
                    <!--Buttons-Cart-->
                    <a style="padding-top: 20px;" href="?action=carrito&id=<?php echo $row["id"]; ?>">
                        <button class="btn btn-primary">Añadir al carrito</button>
                    </a>
                    <!--BuyButton-->
                    <a href="?action=carrito&id=<?php echo $row["id"]; ?>">
                        <button class="btn btn-success">Comprar</button>
                    </a>
                </div>
            </div>
        </div>
        <!--Mostrar Imágenes alternativas--><!--
            <div class="row">
                <div class="col-sm-1">
                    <img class="img-thumbnail" src="producto/img4.png" alt="">
                </div>
                <div class="col-sm-1">
                    <img class="img-thumbnail" src="producto/img4.png" alt="">
                </div>
                <div class="col-sm-1">
                    <img class="img-thumbnail" src="producto/img4.png" alt="">
                </div>
                <div class="col-sm-1">
                    <img class="img-thumbnail" src="producto/img4.png" alt="">
                </div>
            </div>-->
        <!--Mostrar Imágenes alternativas-->
    </div>
    <!-- productos-end-->
    <script type="text/javascript">
                        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('myImg');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    </script>
<!-- Details End -->
