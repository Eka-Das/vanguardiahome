<?php
	if(isset($_SESSION['carrito'])){
		if(isset($_GET['id'])){
			//Definiendo variables que se usarán más adelante
			$arreglo=$_SESSION['carrito'];//encierra a la sesion en una variable para obtener sus datos como un arreglo
			$encontro=false;//le otorga el valor false a la variable
			$numero=0;

			for($i=0;$i<count($arreglo);$i++){
				if($arreglo[$i]['Id']==$_GET['id']){
					$encontro=true;
					$numero=$i;
				}
			}
			//Define la cantidad y suma 1 al último producto producto id
			if($encontro==true){
				$arreglo[$numero]['Cantidad']=$arreglo[$numero]['Cantidad']+1;
				$_SESSION['carrito']=$arreglo;
			}else{
				$nombre="";
				$precio=0;
				$imagen="";
				$id = $_GET['id'];
				$sql = "SELECT * FROM productos WHERE id = $id";
				$res = $conexion->query($sql);

				while ($f=mysqli_fetch_array($res)) {
					$nombre=$f['product_name'];
					$precio=$f['product_price'];
					$imagen=$f['product_img'];
				}

				$datosNuevos=array('Id'=>$id,
								'Nombre'=>$nombre,
								'Precio'=>$precio,
								'Imagen'=>$imagen,
								'Cantidad'=>1);
				//añade datos nuevos al final del arreglo $arreglo osea añade un nuevo producto a la lista del carrito
				array_push($arreglo, $datosNuevos);
				$_SESSION['carrito']=$arreglo;

			}
		}
	}else{
		echo "no hay session";
		if(isset($_GET['id'])){
			$nombre="";
			$precio=0;
			$imagen="";
			$id = $_GET['id'];
			$sql="SELECT * FROM productos WHERE id=$id";
			$re = mysqli_query($conexion, $sql);

			while ($f=mysqli_fetch_array($re)) {
				$nombre=$f['product_name'];
				$precio=$f['product_price'];
				$imagen=$f['product_img'];
			}
			$arreglo[]=array('Id'=>$_GET['id'],
							'Nombre'=>$nombre,
							'Precio'=>$precio,
							'Imagen'=>$imagen,
							'Cantidad'=>1);
			$_SESSION['carrito']=$arreglo;
		}
	}
?>
		<link rel="stylesheet" type="text/css" href="../dist/css/haostyle.css">
	<body onload="aviso()" style="border-style: none;">
		<script type="text/javascript">
			function aviso(){
				alert("Ingrese sus datos para confirmar el pedido");
			}
		</script>

        <div class="container">
            <div class="panel panel-default">

                <div class="panel-body">


					<div style="float: right;" class="col-md-3">
					<div class="card">
						<article class="card-body mx-auto" style="max-width: 400px;">
							<h4 class="card-title mt-3 text-center">Registrarse</h4>
							<form action="carrito/registrando.php" method="POST">
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-user"></i> </span>
								 </div>
								<input required="" name="name" class="form-control" placeholder="Nombre completo" type="text">
							</div> <!-- form-group// -->
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
								 </div>
								<input required="" name="email" class="form-control" placeholder="Correo" type="email">
							</div> <!-- form-group// -->
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-phone"></i> </span>
								</div>
								<input required="" name="phone" class="form-control" placeholder="Teléfono" type="text">
							</div> <!-- form-group// -->
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-building"></i> </span>
								</div>
								<select style="width: 100%;" required="" name="profesion" class="form-control">
									<option selected="">Profesión</option>
									<option>Arquitecto</option>
									<option>Diseñador(a)</option>
									<option>Gerente</option>
									<option>Contabilidad</option>
								</select>
							</div> <!-- form-group end.// -->
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-block"> Confirmar pedido</button>
							</div> <!-- form-group// -->
						</form>
						</article>
					</div> <!-- card.// -->
					</div>
                	<!--tnt-->
									<div class="card">
									<table class="table table-hover shopping-cart-wrap">
									<thead class="text-muted">
									<tr>
									  <th scope="col">Producto</th>
									  <th scope="col" width="120">Cantidad</th>
									  <th scope="col" width="120">Precio</th>
									  <th scope="col" width="120">Subtotal</th>
									</tr>
									</thead>
									<tbody>
										<?php
                            $total=0;
                                if(isset($_SESSION['carrito'])){
                                $datos=$_SESSION['carrito'];

                                $total=0;
                            ?>
											<?php
				                                for($i=0;$i<count($datos);$i++){
				                            ?>
									<tr>
										<td>
									<figure class="media">
										<div class="img-wrap"><img src="data:image/jpg;base64,<?php echo base64_encode($datos[$i]['Imagen']);?>" class="img-thumbnail img-sm"></div>
										<figcaption class="media-body">
											<h6 class="title text-truncate"><?php echo $datos[$i]['Nombre'];?></h6>
											<dl class="dlist-inline small">
											  <dt>Size: </dt>
											  <dd>XXL</dd>
											</dl>
											<dl class="dlist-inline small">
											  <dt>Color: </dt>
											  <dd>Orange color</dd>
											</dl>
										</figcaption>
									</figure>
										</td>
										<td>
											<center>
												<p><?php echo $datos[$i]['Cantidad'];?></p>
											</center>

										</td>
										<td>
											<div class="price-wrap">
												<var class="price"><?php echo $datos[$i]['Precio'].' MXN'; ?></var>
												<small class="text-muted">c/u
                                        </small>
											</div> <!-- price-wrap .// -->
										</td>
										<td>
											<?php
                                        $subtotal = ($datos[$i]['Precio']*$datos[$i]['Cantidad']);
                                        echo $subtotal.'$'.' MXN';
                                        ?>
                                        	<?php
                                            $total=($datos[$i]['Cantidad']*$datos[$i]['Precio'])+$total;
                                            }
                                        ?>
												</td>
											</tr>

										<?php

                      }else{
                      echo '<center><h2>No has añadido ningun producto</h2></center>';
                      }
                     ?>
										</tbody>
										<tfoot>

										</tfoot>
										</table>
										<div class="row">
                                            <div class="col-md-3"></div>
											<div class="col-md-6">
		                  	<?php if($total!=0){
		                      echo '<center><h2 id="total">Total: '.$total.'</h2></center>';
		                    }
		                    ?>
		                     <a href="../index.php" class="btn btn-primary"><i class="glyphicon glyphicon-menu-left"></i> Continue Comprando</a>
											</div>
										</div>
									</div> <!-- tnt// -->

									<div class="container">
									</div>
            <section class="section-main bg padding-y-sm">
            <div class="container">
                <div class="card">
                <div class="card-body">
                <div class="row row-sm">
                  <aside class="col-md-3">
                        </aside>
                            <div class="col-md-6">
                                <aside class="col-md-3">
                                </aside>
                                </div>
                    </div> <!-- card-body .// -->
                    </div> <!-- card.// -->
                            <!--
                                <figure class="mt-3 banner p-3 bg-secondary">
                                    <div class="text-lg text-center white">Useful banner can be here</div>
                                </figure>
                            -->
                </div> <!-- container .//  -->
            </section>

			</div>

		</div>
