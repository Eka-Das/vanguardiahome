<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pagos - PHP Carrito de compras Tutorial</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
    .container{padding: 20px;}
    .table{width: 65%;float: left;}
    .shipAddr{width: 30%;float: left;margin-left: 30px;}
    .footBtn{width: 95%;float: left;}
    .orderBtn {float: right;}
    </style>
</head>
<body>
<div class="container">
<div class="panel panel-default">
<div class="panel-heading"> 

<ul class="nav nav-pills">
  <li role="presentation"><a href="../">Inicio</a></li>
  <li role="presentation"><a href="VerCarta.php">Ver carrito</a></li>
  <li role="presentation" class="active"><a href="Pagos.php">Pagos</a></li>
</ul>
</div>

<div class="panel-body">
    <h1>Vista previa de la Orden</h1>
    <table class="table">
    <thead>
        <tr>
            <th>Producto</th>
            <th>Pricio</th>
            <th>Cantidad</th>
            <th>Sub total</th>
        </tr>
    </thead>
    <tbody>
        <?php
    include "../administracion/login/conexion.php";

            $sql="SELECT * FROM compras";
            $re = mysqli_query($conexion, $sql);
            $numeroventa=0;

            while ($f=mysqli_fetch_array($re)) {
                    if($numeroventa !=$f['numeroventa']){
                        echo '<tr><td>Compra Número: '.$f['numeroventa'].' </td></tr>';
                    }
                    $numeroventa=$f['numeroventa'];
                    echo '<tr>
                        <td><img src="./productos/'.$f['imagen'].'" width="100px" heigth="100px" /></td>
                        <td>'.$f['nombre'].'</td>
                        <td>'.$f['precio'].'</td>
                        <td>'.$f['cantidad'].'</td>
                        <td>'.$f['subtotal'].'</td>

                    </tr>';
            }
            ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            
            <td class="text-center"><strong>Total</strong></td>
            
        </tr>
    </tfoot>
    </table>
    <div class="shipAddr">
        <h4>Detalles de envío</h4>
        <p></p>
        <p><?php //echo $custRow['email']; ?></p>
        <p><?php //echo $custRow['phone']; ?></p>
        <p><?php //echo $custRow['address']; ?></p>
    </div>
    <div class="footBtn">
        <a href="../" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Continue Comprando</a>
        <a href="AccionCarta.php?action=placeOrder" class="btn btn-success orderBtn">Realizar pedido <i class="glyphicon glyphicon-menu-right"></i></a>
    </div>
        </div>
 <div class="panel-footer">PcLabs.Tech</div>
 </div><!--Panek cierra-->
</div>
</body>
</html>