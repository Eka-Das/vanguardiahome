<?php
    
    if(isset($_SESSION['carrito'])){
        if(isset($_GET['id'])){
            //Definiendo variables que se usarán más adelante
            $arreglo=$_SESSION['carrito'];//encierra a la sesion en una variable para obtener sus datos como un arreglo
            $encontro=false;//le otorga el valor false a la variable
            $numero=0;

            for($i=0;$i<count($arreglo);$i++){
                if($arreglo[$i]['Id']==$_GET['id']){
                    $encontro=true;
                    $numero=$i;
                }
            }

            if($encontro==true){
                $arreglo[$numero]['Cantidad']=$arreglo[$numero]['Cantidad']+1;
                $_SESSION['carrito']=$arreglo;
            }else{
                $nombre="";
                $precio=0;
                $imagen="";
                $id = $_GET['id'];
                $sql = "SELECT * FROM productos WHERE id = $id";
                $res = $conexion->query($sql);

                while ($f=mysqli_fetch_array($res)) {
                    $nombre=$f['product_name'];
                    $precio=$f['product_price'];
                    $imagen=$f['product_img'];
                    $detalles = $f['product_description'];
                }
                $datosNuevos=array('Id'=>$id,
                                'Nombre'=>$nombre,
                                'Precio'=>$precio,
                                'Imagen'=>$imagen,
                                'Detalles'=>$detalles,
                                'Cantidad'=>1);

                array_push($arreglo, $datosNuevos);
                $_SESSION['carrito']=$arreglo;

            }
        }
    }else{
        echo "no hay sesion";
        if(isset($_GET['id'])){
            $nombre="";
            $precio=0;
            $imagen="";
            $id = $_GET['id'];
            $sql="SELECT * FROM productos WHERE id=$id";
            $re = mysqli_query($conexion, $sql);

            while ($f=mysqli_fetch_array($re)) {
                $nombre=$f['product_name'];
                $precio=$f['product_price'];
                $imagen=$f['product_img'];
                $detalles=$f['product_description'];
            }
            $arreglo[]=array('Id'=>$_GET['id'],
                            'Nombre'=>$nombre,
                            'Precio'=>$precio,
                            'Imagen'=>$imagen,
                            'Detalles'=>$detalles,
                            'Cantidad'=>1);
            $_SESSION['carrito']=$arreglo;
        }
    }
?>
        <!-- ========================= SECTION MAIN ========================= -->
        	<section class="section-main bg padding-y-sm">
                <div class="container">
                    <header class="section-heading heading-line">
                        <h4 class="title-section bg text-uppercase">Carrito</h4>
                    </header><!---->
                    <!--
                        <div class="card">
                            <div class="card-body">
                                <div class="row row-sm">
                                    
                                    <aside class="col-md-3">
                                    </aside>
                                    <div class="col-md-6">
                                    <aside class="col-md-3">
                                    </aside>
                                </div>
                            </div> 
                        </div> 

                        <figure class="mt-3 banner p-3 bg-secondary">
                            <div class="text-lg text-center white">Useful banner can be here</div>
                        </figure>
                    -->
                </div>
        	</section>
        <!-- ========================= SECTION MAIN END// ========================= -->
        
        <!-- ========================= SECTION ITEMS ========================= -->
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                	<!--tnt-->
                    <div class="card">
                        <table class="table table-hover shopping-cart-wrap">
                            <thead class="text-muted">
                                <tr>
                                    <th scope="col" width="200">Producto</th>
                                    <th scope="col" width="80">Cantidad</th>
                                    <th scope="col" width="120">Precio</th>
                                    <th scope="col" width="120">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total=0;
                                    if(isset($_SESSION['carrito'])){
                                        $datos=$_SESSION['carrito'];
                                        $total=0;
                                        
                                        for($i=0;$i<count($datos);$i++){
                                            ?>
                                                <tr>
                                                    <td>
                                                        <figure class="media">
                                                            <div class="img-wrap"><img src="data:image/jpg;base64,<?php echo base64_encode($datos[$i]['Imagen']);?>" class="img-thumbnail img-sm"></div>
                                                            <figcaption class="media-body">
                                                                <h6 class="title text-truncate" style="text-transform: uppercase;"><?php echo $datos[$i]['Nombre'];?></h6>
                                                                <dl class="dlist-inline small">
                                                                    <p><?php echo $datos[$i]['Detalles'];?></p>
                                                                </dl><!--
                                                                <dl class="dlist-inline small">
                                                                    <dt>Color: </dt>
                                                                    <dd>Orange color</dd>
                                                                </dl>-->
                                                            </figcaption>
                                                        </figure>
                                                    </td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option><?php echo $datos[$i]['Cantidad'];?></option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrap">
                                                            <var class="price"><?php echo $datos[$i]['Precio'].' MXN'; ?></var>
                                                            <small class="text-muted">(c/u)
                                                    </small>
                                                        </div> <!-- price-wrap .// -->
                                                    <td>
                                                        <?php
                                                    $subtotal = ($datos[$i]['Precio']*$datos[$i]['Cantidad']);
                                                    echo $subtotal.'$'.' MXN';
                                                    ?>
                                                        <?php
                                                        $total=($datos[$i]['Cantidad']*$datos[$i]['Precio'])+$total;
                                                    ?>
                                                            </td>
                                                    </td>
                                                            <td class="text-right">
                                                            <a title="" href="" class="btn btn-outline-success" data-toggle="tooltip" data-original-title="Save to Wishlist"> <i class="fa fa-heart"></i></a>
                                                            <a href="" class="btn btn-outline-danger"> × Remove</a>
                                                        
                                                            </td>
                                            <?php
                                        }
                                        ?>
                                <?php
                                    }else{
                                    echo '<center><h2>No has añadido ningun producto</h2></center>';
                                    }
                                ?>
                            </tr>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                        <div class="row">
                            <div class="col-md-6">
                                <?php if($total!=0){
                                    echo '<center><h2 id="total">Total: '.$total.'</h2></center>';
                                    }
                                ?>
                                <a href="index.php" class="btn btn-primary">
                                    <i class="glyphicon glyphicon-menu-left"></i>
                                        Continue Comprando
                                </a>

                                <a style="float: right;" href="?action=confirmarPedido" class="btn btn-success ">
                                    Hacer pedido <i class="glyphicon glyphicon-menu-right"></i>
                                </a>

                            </div>
                        </div>
                    </div> <!-- tnt// -->
                </div>
            </div>
        </div>