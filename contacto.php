<section id="contact">
  <div class="container">
    <div class="well well-sm">
      <h3><strong>Visítenos</strong></h3>
    </div>
	
	<div class="row">
	  <div class="col-md-7">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14939.426788089913!2d-103.4143524!3d20.593909!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x67d28a05d747a3fd!2sHome+and+office.store!5e0!3m2!1ses-419!2smx!4v1548417533997" width="100%" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

      <div class="col-md-5">
          <h4><strong>Déjenos sus datos.</strong></h4>
          <p>Nos pondremos en contacto con usted.</p>
        <form>
          <div class="form-group">
            <input type="text" class="form-control" name="" value="" placeholder="Nombre">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="" value="" placeholder="Correo Electrónico">
          </div>
          <div class="form-group">
            <input type="tel" class="form-control" name="" value="" placeholder="Teléfono">
          </div>
          <div class="form-group">
            <textarea class="form-control" name="" rows="3" placeholder="Mensaje"></textarea>
          </div>
          <button class="btn btn-default" type="submit" name="button">
              <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Enviar
          </button>
        </form>
      </div>
    </div>
  </div>
</section>